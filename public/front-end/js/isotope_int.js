
$(function(){
		
var is_refreshed = 1;
// init Isotope
    var $grid = $('.item-container').isotope({
        itemSelector: '.items',
        layoutMode: 'fitRows',
        // getSortData: {
        //     name: '.name',
        //     symbol: '.symbol',
        //     number: '.number parseInt',
        //     category: '[data-category]',
        //     weight: function( itemElem ) {
        //         var weight = $( itemElem ).find('.weight').text();
        //         return parseFloat( weight.replace( /[\(\)]/g, '') );
        //     }
        // }
    });

// filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
        }
    };

// bind filter button click
    $('.page-bottom-content').on( 'click', '.category-button', function() {
        if(!$("#detail-subcontent").hasClass('d-none')){
            $("#items-subcontent").removeClass('d-none');
            $("#detail-subcontent").addClass('d-none');
        }
		if(!is_refreshed){
			$('#remove_selection').trigger('click');			
		}
		$('.category-button').removeClass('active');
        $(this).addClass('active');
        var filterValue = $( this ).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[ filterValue ] || filterValue;
        $grid.isotope({ filter: filterValue });
		is_refreshed =0;
    });

    //$('.category-button:first').trigger('click');


    $('.category-button').mouseenter(function() {
        $(this).find('img').addClass('gray-svg');
    });
    $('.category-button').mouseleave(function() {
        $(this).find('img').removeClass('gray-svg');
    });


});
