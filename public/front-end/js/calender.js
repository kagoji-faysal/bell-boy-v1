var startDay = 0;
var calenderDayLimit = 31;
function orderCalender(startDay) {
    // moment().calendar(null, {
    //     sameDay: '[Today]',
    //     nextDay: '[Tomorrow]',
    //     nextWeek: 'dddd',
    //     lastDay: '[Yesterday]',
    //     lastWeek: '[Last] dddd',
    //     sameElse: 'DD/MM/YYYY'
    // });
    var calenderLimit = 0;
    var calenderHtml = '';
    calenderHtml += '<div class="box box-success partnerServiceCatalogueDiv"><div class="box-header"><h5 class="box-title">Select Date</h5>'+
        '</div><div class="box-body">';
    calenderLimit = 7 + startDay;
    if(calenderLimit > calenderDayLimit){
        calenderLimit = calenderDayLimit;
    }
    var today = moment().format('L');
    var selectedDateT = $('#dateModal').find('.order_date').val();
    for(var i=startDay; i < calenderLimit; i++){
        var ca= moment().add(i, 'days').calendar('DD/MM/YYYY');
        var ca_date_format= moment().add(i, 'days').format('ll');
        ca_date_format = ca_date_format.substring(0, ca_date_format.indexOf(','))
        var ca_split = ca_date_format.split(' ');
        calenderHtml += '<a class="btn btn-app calender_date_ ' + (today == ca ? 'todayColor' : '') + ' ' + (selectedDateT == ca ? 'selectedColor' : '') +  '" href="#" id="'+ ca +'">'+
            // '<i class="fa fa-calendar-o fa-2x"></i> <br> '+ ca;
            '<span class="calender_split">' +  ca_split[0] + '<br> <br> '+ ca_split[1] + '</span>';
    }
    calenderHtml +=  '</div>'+
        '</div>';

    if(startDay > 0 && calenderLimit < calenderDayLimit){
        $('#dateModal').find('.box-body').fadeOut(1000, function () {
            $('#dateModal').find('.date_calender').html('<a href="#" class="previous calenderIconPrev"><span class="fa-4x glyphicon glyphicon-chevron-left "></span></a>' + calenderHtml+'<a href="#" class="previous calenderIcon"><span class="fa-4x glyphicon glyphicon-chevron-right "></span></a>');
        });
    } else if(calenderLimit ==  calenderDayLimit){
        $('#dateModal').find('.box-body').fadeOut(1000, function () {
            $('#dateModal').find('.date_calender').html('<a href="#" class="previous calenderIconPrev"><span class="fa-4x glyphicon glyphicon-chevron-left "></span></a>' + calenderHtml);
        });
    } else {
        $('#dateModal').find('.box-body').fadeOut(1000);
        $('#dateModal').find('.date_calender').html(calenderHtml+'<a href="#" class="previous calenderIcon"><span class="fa-4x glyphicon glyphicon-chevron-right"></span></a>');
    }
}

function updateSelectedDate(){
    var selectedDate = $('#dateModal').find('.order_date').val();
    // console.log(selectedDate)
    var today = moment().format('L');
    if(selectedDate){
        $('#dateModal').find('.calender_date_').each(function (i,j) {
            if(today == $(this).attr('id') && !$(this).hasClass('selectedColor')){
                $(this).addClass('todayColor')
            }

            if(selectedDate == today){
                $(this).removeClass('todayColor');
            }

            // if($(this).attr('id') == selectedDate){
            //     $(this).addClass('selectedColor')
            // }
        });
    }
}

function getCurrentHour(){
    const now = moment()
    var today_hour = now.format("HH:mm:ss") // 13:00:00
    var explode_today_hour = today_hour.split(':');
    $('#dateModal').find('.order_hour_from').val(explode_today_hour[0])
    $('#dateModal').find('.order_min_from').val(explode_today_hour[1])
}


$(function(){
    getCurrentHour()
    $('#dateModal').on('click','.calenderIcon', function (e) {
        e.preventDefault();
        startDay += 7;
        orderCalender(startDay);
        updateSelectedDate();
    });

    $('#dateModal').on('click','.calenderIconPrev', function (e) {
        e.preventDefault();
        startDay -= 7;
        orderCalender(startDay);
        updateSelectedDate();
    });

    $('#dateModal').on('click','.calender_date_', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if($(this).hasClass('todayColor')){
            $('#dateModal').find('.calender_date_').removeClass('todayColor');
        }
        $('#dateModal').find('.calender_date_').removeClass('selectedColor');
        $(this).addClass('selectedColor');
        $('#dateModal').find('.order_date').val($(this).attr('id'));
        updateSelectedDate();

        $('#dateModal').find('.selected_order_date').html(moment($(this).attr('id')).format('Do MMM YY') + ' | ')
        $('#dateModal').find('.order_time_div').trigger('input');
    });

    $('#dateModal').on('input','.order_time_div', function (e) {
        var order_hour_from = $('#dateModal').find('.order_hour_from').val()
        var order_min_from = $('#dateModal').find('.order_min_from').val();
        var order_hour_to = ('0' + (order_hour_from != 23 ? (parseInt(order_hour_from) + 1) : '00')).slice(-2);
        var order_min_to = $('#dateModal').find('.order_min_from').val()
        var order_time = (order_hour_from ? order_hour_from : '00') + ' : ' + (order_min_from ? order_min_from : '00') + ' - ' + (order_hour_to ? order_hour_to : '00') + ' : ' + (order_min_to ? order_min_to : "00");
        $('#dateModal').find('.order_time').val(order_time);
        $('#dateModal').find('.selected_order_time').html(order_time);
    });


    $("#dateModal").on("hidden.bs.modal", function(){
        // $(this).find('.selectedAttr').empty();
        $('#dateModal').find('.selected_order_date').empty()
        $('#dateModal').find('.selected_order_time').empty()
        $('#dateModal').find('.order_date').val("")
        $('#dateModal').find('.order_time').val("")
        $('#dateModal').find('.option_calender').prop('selectedIndex',0)
        $('#dateModal').find('.calender_date_').removeClass('selectedColor');
        startDay = 0;
    });
});
