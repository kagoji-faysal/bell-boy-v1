
$(function(){
    var site_url = $('.site_url').val();
    var table_added = 0;

    $('.serviceTbl').on('click', '.addMoreAttrBtn', function (e) {
        e.preventDefault();
        var this_id = $(this).attr('id')
        window.location.href = site_url+'/partner/attribute-generate/' + this_id;
    });

    $('.partnerServiceListTbl').on('click', '.addMoreAttrBtn', function (e) {
        e.preventDefault();
        var this_id = $(this).attr('id')
        window.location.href = site_url+'/partner/attribute-generate/' + this_id;
    });

    //add more attribute type value and price
    $('.addServiceAttributeForm').on('click', '.addMoreAttrValue', function () {
        var cloneVal = $('.cloneAttrTbl tbody tr.attributeValueClone').clone()
        var attrTypeSize = 0;
        var attrValSize = 0;//$(this).parents('tbody').find('.service_pricing_type_value').size();
        var arr_nam = 0
        var attrTblIndex = $(this).parents('tbody').find('.tbl_serial').val();
        //work
        var numberPattern = /\d+/g;
        var prev_fi = $(this).parents('tr').next(".attributeValueClone").find('.attribute_field_value').attr('name');
        console.log(prev_fi)
        if(typeof prev_fi == 'undefined'){
            prev_fi = $(this).parents('tr').find('.attribute_field_value').attr('name');
            arr_nam = prev_fi.match( numberPattern )
            attrValSize = 1;
            attrTypeSize = parseInt(arr_nam[1]);
        } else {
            arr_nam = prev_fi.match( numberPattern )
            attrValSize = parseInt(arr_nam[2]) + 1;
            attrTypeSize = parseInt(arr_nam[1]);
        }

        $(cloneVal).find('.attribute_field_value').attr('name', 'attribute_field_value[' + attrTblIndex + ']['+ attrTypeSize +']['+attrValSize+']');
        $(cloneVal).find('.service_pricing_type_value').attr('name', 'service_pricing_type_value[' + attrTblIndex + ']['+ attrTypeSize +']['+attrValSize+']');
        $(cloneVal).find('.service_pricing_type_cost').attr('name', 'service_pricing_type_cost[' + attrTblIndex + ']['+ attrTypeSize +']['+attrValSize+']');
        $(cloneVal).find('.is_selected').attr('name', 'is_selected[' + attrTblIndex + ']['+ attrTypeSize +']['+attrValSize+']');
        $(cloneVal).fadeIn(1000).insertAfter($(this).parents('tr'));
    });

    //add more attribute type
    $('.addServiceAttributeForm').on('click', '.addMoreAttrType', function () {
        var cloneAtrval = $('.cloneAttrTbl tbody tr.attributeTypeClone').clone();
        var lastAttrTypeName = $(this).parents('tbody').find('.service_pricing_type_id').last().attr('name');
        var attrTblIndex = $(this).parents('tbody').find('.tbl_serial').val();
        var numberPattern = /\d+/g;
        var arr_nam = lastAttrTypeName.match(numberPattern)
        var attrTypeSize = parseInt(arr_nam[1]) + 1;

        $(cloneAtrval).find('.attribute_type_name').attr('name', 'attribute_type_name[' + attrTblIndex + ']['+ attrTypeSize +']');
        $(cloneAtrval).find('.service_pricing_type_id').attr('name', 'service_pricing_type_id[' + attrTblIndex + ']['+ attrTypeSize +']');
        $(cloneAtrval).find('.attribute_field_value').attr('name', 'attribute_field_value[' + attrTblIndex + ']['+ attrTypeSize +'][0]');
        $(cloneAtrval).find('.service_pricing_type_value').attr('name', 'service_pricing_type_value[' + attrTblIndex + ']['+ attrTypeSize +'][0]');
        $(cloneAtrval).find('.service_pricing_type_cost').attr('name', 'service_pricing_type_cost[' + attrTblIndex + ']['+ attrTypeSize +'][0]');
        $(cloneAtrval).find('.is_selected').attr('name', 'is_selected[' + attrTblIndex + ']['+ attrTypeSize +'][0]');
        $(this).parents('tbody').append($(cloneAtrval).fadeIn(1000))
    });


    //ad more attribute with class
    $('.addServiceAttributeForm').on('click', '.addMoreAttrsBtn', function () {
        if(table_added == 0){
            table_added = parseInt($('.addServiceAttributeForm .tbl_serial').last().val());
        }
        table_added= table_added+1;
        var cloneTbl = $('.tableDiv .attributeTable').clone();
        var attrTableSize = $('.addServiceAttributeForm').find('.attributeTable').size();
        $(cloneTbl).find('.attribute_class_name').attr('name', 'attribute_class_name[' + table_added + ']');
        $(cloneTbl).find('.service_pricing_class_id').attr('name', 'service_pricing_class_id[' + table_added + ']');
        $(cloneTbl).find('.attribute_type_name').attr('name', 'attribute_type_name[' + table_added + '][0]');
        $(cloneTbl).find('.service_pricing_type_id').attr('name', 'service_pricing_type_id[' + table_added + '][0]');
        $(cloneTbl).find('.attribute_field_value').attr('name', 'attribute_field_value[' + table_added + '][0][0]');
        $(cloneTbl).find('.service_pricing_type_value').attr('name', 'service_pricing_type_value[' + table_added + '][0][0]');
        $(cloneTbl).find('.service_pricing_type_cost').attr('name', 'service_pricing_type_cost[' + table_added + '][0][0]');
        $(cloneTbl).find('.is_selected').attr('name', 'is_selected[' + table_added + '][0][0]');
        $(cloneTbl).find('.attrTitle').html('Attribute ' + (attrTableSize + 1));
        $(cloneTbl).find('.tbl_serial').val(table_added);
        $('.addServiceAttributeForm').append("<hr class='half-rule'>")
        $('.addServiceAttributeForm').append( $(cloneTbl).fadeIn(1000));

    });

    $('.addServiceAttributeForm').on('click', '.removeMoreAttrType', function () {
        var attr_val_id = $(this).attr('attr_typ_id');
        if(attr_val_id){
            var service_pricing_item_id = $('.service_pricing_item_id').val();
            $.post(site_url + '/partner/delete-attribute', {'field' : 'service_pricing_type_id', 'attr_id' : attr_val_id, 'service_pricing_item_id' : service_pricing_item_id}, function (data) {
                $.notify(data.msg, data.status);
            }, 'json')
        }
        $(this).parents('tr').nextUntil(".attributeTypeClone").andSelf().fadeOut(1000, function(){
            $(this).remove();
        });;
    });


    $('.addServiceAttributeForm').on('click', '.removeMoreAttrValue', function () {
        var attr_val_id = $(this).attr('attr_val_id');
        if(attr_val_id){
            var service_pricing_item_id = $('.service_pricing_item_id').val();
            $.post(site_url + '/partner/delete-attribute', {'field' : 'service_pricing_type_value', 'attr_id' : attr_val_id, 'service_pricing_item_id' : service_pricing_item_id}, function (data) {
                $.notify(data.msg, data.status);
            }, 'json')
        }
        $(this).parents('tr').fadeTo("slow",0.7, function(){
            $(this).remove();
        })
    });



    $('.addServiceAttributeForm').on('click', '.removeMoreAttrsBtn', function () {
        var attr_val_id = $(this).attr('attr_class_id');
        if(attr_val_id){
            var service_pricing_item_id = $('.service_pricing_item_id').val();
            $.post(site_url + '/partner/delete-attribute', {'field' : 'service_pricing_class_id', 'attr_id' : attr_val_id, 'service_pricing_item_id' : service_pricing_item_id}, function (data) {
                $.notify(data.msg, data.status);
            }, 'json')
        }
        $(this).parents('.attributeTable').prev('.half-rule').remove();
        $(this).parents('.attributeTable').fadeOut(1000, function(){
            $(this).remove();
            $('.addServiceAttributeForm').find('.attrTitle').each(function (i, j) {
                    $(this).html('Attribute ' + (i+1));
            });
        });
    });


    //attribute autocomplete
    $(".addServiceAttributeForm").on('focus', '.attribute_class_name', function (event) {
        var This = $(this)
        $.get(site_url+'/partner/attr-class-auto-suggestion', function(data){
            This.autocomplete({
                source: function(req, response) {
                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    var matcher = new RegExp( "^" + re, "i" );
                    response($.grep( data, function(item){
                        return matcher.test(item.value); }) );
                },
                select: function( event, ui ) {
                    // console.log(event);
                    This.parents('tr').find('.service_pricing_class_id').val( ui.item.id )
                    $(this).trigger('change');
                },
                change: function (event, ui) {
                    // console.log(ui.item);
                    if (ui.item === null) {
                        This.parents('tr').find('.service_pricing_class_id').val('');
                    }
                }
            });
        }, 'json');
    });

    //attribute type autocomplete
    $(".addServiceAttributeForm").on('focus', '.attribute_type_name', function (event) {
        var This = $(this);
        var attr_class_id = $(this).parents('tbody').find('.service_pricing_class_id').val();
        // console.log(attr_class_id)
        if(attr_class_id) {
            $.get(site_url + '/partner/attr-type-auto-suggestion/' + attr_class_id, function (data) {
                This.autocomplete({
                    source: function (req, response) {
                        var re = $.ui.autocomplete.escapeRegex(req.term);
                        var matcher = new RegExp("^" + re, "i");
                        response($.grep(data, function (item) {
                            return matcher.test(item.value);
                        }));
                    },
                    response: function(event, ui){
                        if(ui.content.length == 0){
                            This.parents('tr').find('.service_pricing_type_id').val('');
                        }
                    },
                    select: function (event, ui) {
                        This.parents('tr').find('.service_pricing_type_id').val(ui.item.id)
                        $(this).trigger('change');
                    },
                    change: function (event, ui) {
                        if (ui.item === null) {
                            This.parents('tr').find('.service_pricing_type_id').val('');
                        }
                    }
                });
            }, 'json');
        }
    });

    //attribute value autocomplete
    $(".addServiceAttributeForm").on('focus', '.attribute_field_value', function (event) {
        var This = $(this);
        var attr_class_id = $(this).parents('tbody').find('.service_pricing_class_id').val();
        var attr_type_id = $(this).parents('tr').find('.service_pricing_type_id').val();

        if($(this).parents('tr').find('.service_pricing_type_id').length == 0){
            attr_type_id = $(this).parents('tr').prev('.attributeTypeClone').find('.service_pricing_type_id').val();
        }

        if(attr_class_id && attr_type_id) {
            $.get(site_url + '/partner/attr-val-auto-suggestion/' + attr_class_id + '/' + attr_type_id, function (data) {
                This.autocomplete({
                    source: function (req, response) {
                        console.log(data)
                        var re = $.ui.autocomplete.escapeRegex(req.term);
                        var matcher = new RegExp("^" + re, "i");
                        response($.grep(data, function (item) {
                            return matcher.test(item.value);
                        }));
                    },
                    response: function(event, ui){
                        if(ui.content.length == 0){
                            This.parents('tr').find('.service_pricing_type_value').val('');
                        }
                    },
                    select: function (event, ui) {
                        This.parents('tr').find('.service_pricing_type_value').val(ui.item.id)
                        $(this).trigger('change');
                    },
                    change: function (event, ui) {
                        if (ui.item === null) {
                            This.parents('tr').find('.service_pricing_type_value').val('');
                        }
                        return false;
                    },
                    minLength: 3,
                });
            }, 'json');
        }
    });


    //save attribute data
    $('.saveServiceAttributeBtn').on('click', function (e) {
        e.preventDefault();
        $.post(site_url + '/partner/save-attribute', $('.addServiceAttributeForm').serialize(), function (data) {
            if(data.status == 'success'){
                window.location.href = site_url+ data.url;
                $.notify(data.msg, data.status);
            } else {
                $.notify(data.msg, data.status);
            }
        }, 'json')
    })


});
