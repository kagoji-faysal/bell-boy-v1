
<li class="{{isset($page_title) && ($page_title=='Partner Service') ? 'active' : ''}} ">
    <a href="{{url('/partner/services')}}"><i class="clip-cog-2"></i>
        <span class="title"> Services </span>
    </a>
</li>
<li class="{{isset($page_title) && ($page_title=='Partner Order List') ? 'active' : ''}} ">
    <a href="{{url('partner/service/order')}}"><i class="fa fa-shopping-cart"></i>
        <span class="title"> Order </span>
        <span class="selected"></span>
    </a>
</li>
@if(($ACL->level()>=5)&&($ACL->hasRole('partner')))
    <li class="{{isset($page_title) && ($page_title=='User Management') ? 'active' : ''}} ">
        <a href="javascript:void(0)"><i class="clip-user-plus"></i>
            <span class="title"> User Management </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="{{url('partner/user/management?tab=create_user')}}">
                    <span class="title"> Create User </span>
                </a>
            </li>
            <li>
                <a href="{{url('partner/user/management?tab=blocked_user')}}">
                    <span class="title"> Blocked User </span>
                </a>
            </li>
            <li>
                <a href="{{url('partner/user/management?tab=active_user')}}">
                    <span class="title"> Active User </span>
                </a>
            </li>
            <li>
                <a href="{{url('partner/user/management?tab=assign_permission')}}">
                    <span class="title"> Assign Permission </span>
                </a>
            </li>

        </ul>
    </li>
@endif
<li class="{{(isset($page_title) && (strpos($page_title,'Discount')!== false )) ? 'open':''}}">
    <a href="javascript:void (0)">
        <i class="clip-windy" aria-hidden="true"></i>
        <span class="title"> Discount Manage</span><i class="icon-arrow"></i>
        <span class="selected"></span>
    </a>
    <ul class="sub-menu" style="display: {{( isset($page_title) && (strpos($page_title,'Discount') !== false) ) ? 'block':'none'}};">
        <li class="{{isset($page_title) && ($page_title=='Discount List') ? 'active' : ''}}">
            <a href="{{url('/partner/discount/list')}}">
                <i class="fa fa-money"></i>
                <span class="title"> Add Flat Discount</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="{{isset($page_title) && ($page_title=='Service Discount List') ? 'active' : ''}}">
            <a href="{{url('/partner/service/discount/list')}}">
                <i class="fa fa-tags"></i>
                <span class="title"> Add Service Discount</span>
                <span class="selected"></span>
            </a>
        </li>
    </ul>
</li>
