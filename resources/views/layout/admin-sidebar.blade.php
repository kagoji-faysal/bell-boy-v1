


<li class="{{isset($page_title) && (strpos($page_title,'Category')!== false ) ? 'active' : ''}} ">
    <a href="{{url('/admin/category')}}"><i class="clip-user-2"></i>
        <span class="title"> Category </span>
        <span class="selected"></span>
    </a>
</li>

@if(($ACL->level()>=5)&&($ACL->hasRole('admin')))
    <li class="{{isset($page_title) && ($page_title=='User Management') ? 'active' : ''}} ">
        <a href="javascript:void(0)"><i class="clip-user-plus"></i>
            <span class="title"> User Management </span><i class="icon-arrow"></i>
            <span class="selected"></span>
        </a>
        <ul class="sub-menu">
            <li>
                <a href="{{url('admin/user/management?tab=create_user')}}">
                    <span class="title"> Create User </span>
                </a>
            </li>
            <li>
                <a href="{{url('admin/user/management?tab=blocked_user')}}">
                    <span class="title"> Blocked User </span>
                </a>
            </li>
            <li>
                <a href="{{url('admin/user/management?tab=active_user')}}">
                    <span class="title"> Active User </span>
                </a>
            </li>
            <li>
                <a href="{{url('admin/user/management?tab=assign_permission')}}">
                    <span class="title"> Assign Permission </span>
                </a>
            </li>

        </ul>
    </li>
@endif

<li class="{{isset($page_title) && ($page_title=='Service Config') ? 'active' : ''}} ">
    <a href="{{url('admin/services')}}"><i class="clip-cog-2"></i>
        <span class="title"> Services </span>
        <span class="selected"></span>
    </a>
</li>
<li class="{{isset($page_title) && (strpos($page_title,'Service Partner')!== false ) ? 'active' : ''}} ">
    <a href="{{url('/admin/service/partner/create')}}"><i class="clip-user-5"></i>
        <span class="title"> Partner </span>
        <span class="selected"></span>
    </a>
</li>

<li class="{{isset($page_title) && (strpos($page_title,'Service Order List')!== false ) ? 'active' : ''}} ">
    <a href="{{url('/admin/service/partner/order')}}"><i class="fa fa-shopping-cart"></i>
        <span class="title"> Order </span>
        <span class="selected"></span>
    </a>
</li>
<li class="{{isset($page_title) && (strpos($page_title,'Settings Meta')!== false ) ? 'active' : ''}} ">
    <a href="{{url('/admin/service/settings-meta/list')}}"><i class="fa fa-wrench"></i>
        <span class="title"> Meta Settings </span>
        <span class="selected"></span>
    </a>
</li>
