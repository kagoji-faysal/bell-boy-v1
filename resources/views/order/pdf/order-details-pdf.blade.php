<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link href="{{ public_path('css/pdf.css') }}" rel="stylesheet">
    </head>
<body>
<div class="container" style="font-family: 'Courier New', Courier, monospace !important; ">
    <div class="logo">
        TECHNOLIFE
    </div>
    <div class="row" style="height: 25%; margin-bottom: 20px;">
        <div class="col-xs-12" >
            <div class="invoice-title" style="margin-bottom: 20px;">
                <h2>Order Details</h2><h3 class="pull-right">Order # {{isset($orderDetails[0]['service_orders_id']) ? str_pad($orderDetails[0]['service_orders_id'], 6, "0", STR_PAD_LEFT) : ''}}</h3>
            </div>
            <div class="row" style="margin-bottom: 20px; height: 100px;">
                <div class="col-xs-6">
                    <strong>Ordered By:</strong><br>
                    {{isset($orderDetails[0]['name']) ? $orderDetails[0]['name'] : ''}}
                </div>
                <div class="col-xs-6 text-right" style="float: right;">
                    <strong>Customer Address:</strong><br>
                    {{isset($orderDetails[0]['service_order_customer_address']) ? $orderDetails[0]['service_order_customer_address'] : ''}}
                </div>
            </div>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-xs-6">
                </div>
                <div class="col-xs-6 text-right" style="float: right;">
                    <strong>Order Date:</strong><br>
                    {{isset($orderDetails[0]['created_at']) ? \Carbon\Carbon::parse($orderDetails[0]['created_at'])->format('j F, Y') : ''}}<br><br>
                    <strong>Service Date:</strong><br>
                    {{isset($orderDetails[0]['service_date']) ? \Carbon\Carbon::parse($orderDetails[0]['service_date'])->format('j F, Y') : '' }}
                    {{ ' | ' }}
                    {{isset($orderDetails[0]['service_time']) ? ($orderDetails[0]['service_time']) : ''}}<br>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3><strong>Order summary</strong></h3>
            <div class="table-responsive" style="border: 0px solid white !important; padding: 0px !important;">
                <table class="table orderDtlPreviewTable" style="font-size: 12px !important; width: 100% !important; padding: 0px !important;">
                    <thead>
                        <tr>
                            <td class="text-right" style="text-align: left !important;"><strong>Service Item</strong></td>
                            <td class="text-center" style="text-align: center !important;"><strong>Quantity</strong></td>
                            <td class="text-center" style="text-align: center !important;"><strong>Price</strong></td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($orderDetails as $key => $data)

                        <tr>
{{--                            <td>{{ $data['service_name'] }}</td>--}}
                            <td class="text-right"style="text-align: left !important;">{{ isset($data['service_pricing_title']) ? $data['service_pricing_title'] : ''  }}</td>
                            <td class="text-center" style="text-align: center !important;">1</td>
                            <td class="text-center" style="text-align: center !important;">{{ isset($data['orders_items_type_cost']) ? $data['service_pricing_currency_unit'] . ' ' .$data['orders_items_type_cost'] : 0}}</td>
                        </tr>
                    @endforeach
                    <tr>
{{--                        <td class="thick-line"></td>--}}
                        <td class="thick-line"></td>
                        <td class="thick-line text-center" style="text-align: center !important;"><strong>Subtotal</strong></td>
                        <td class="thick-line"style="text-align: center !important;">{{ isset($orderDetails[0]['service_order_amount']) ? $orderDetails[0]['service_pricing_currency_unit'] . ' ' .$orderDetails[0]['service_order_amount'] : 0 }}</td>
                    </tr>
                    <tr>
                        <td class="no-line"></td>
                        <td class="no-line text-center"style="text-align: center !important;"><strong>Tax</strong></td>
                        <td class="no-line " style="text-align: center !important;">{{ isset($orderDetails[0]['service_order_tax_amount']) ? $orderDetails[0]['service_pricing_currency_unit'] . ' ' .$orderDetails[0]['service_order_tax_amount'] : 0 }}</td>
                    </tr>
                    <tr>
                        <td class="no-line"></td>
                        <td class="no-line text-center"style="text-align: center !important;"><strong>Total</strong></td>
                        <td class="no-line" style="text-align: center !important;">{{ isset($orderDetails[0]['service_order_grand_total']) ? $orderDetails[0]['service_pricing_currency_unit'] . ' ' .$orderDetails[0]['service_order_grand_total'] : 0 }}</td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>



{{--data-dismiss="modal"--}}

