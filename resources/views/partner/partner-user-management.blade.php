@extends('layout.master')
@section('content')
    <!--SHOW ERROR MESSAGE DIV-->
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row page_row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <!--END ERROR MESSAGE DIV-->
    <div class="row ">
        <div class="col-sm-12">
            <div class="tabbable">
                <ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
                    <li class="{{($tab=='create_user') ? 'active' : ''}}">
                        <a data-toggle="tab" href="#create_user">
                            Create User
                        </a>
                    </li>
                    <li class="{{($tab=='blocked_user') ? 'active' : ''}}">
                        <a data-toggle="tab" href="#blocked_user">
                            Blocked Users
                        </a>
                    </li>
                    <li class="{{$tab=='active_user' ? 'active':''}}">
                        <a data-toggle="tab" href="#active_user">
                            Active User
                        </a>
                    </li>
                    <li class="{{$tab=='assign_permission' ? 'active':''}}">
                        <a data-toggle="tab" href="#assign_permission">
                            Assign Permission
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- PANEL FOR CREATE USER -->
                    <div id="create_user" class="tab-pane {{$tab=='create_user' ? 'active':''}}">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="user-form"  action="{{url('partner/user/create')}}" method="post"
                                       enctype="multipart/form-data" class="user-form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>Account Info</h3>
                                            <hr>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="firstname2" class="control-label">
                                                    Name
                                                    <span class="symbol" aria-required="true"></span>
                                                </label>
                                                <input id="first_name" type="text" placeholder="Name"
                                                       class="form-control" name="name"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="email2" class="control-label">
                                                    Email Address
                                                    <span class="symbol" aria-required="true"></span>
                                                </label>
                                                <input type="email" placeholder="email@example.com" class="form-control" name="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="form-field-select-4">
                                                    Select permissions
                                                </label>
                                                <select multiple="multiple" id="form-field-select-4" class="form-control search-select" name="permissions[]" required>
                                                  @if(isset($permissions) && count($permissions))
                                                        @foreach($permissions as $permis)
                                                            <option value="{{ $permis->id }}">{{ucfirst($permis->name)}}</option>
                                                        @endforeach
                                                  @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="control-label">
                                                    Password
                                                    <span class="symbol" aria-required="true"></span>
                                                </label>
                                                <input type="password" name="password" placeholder="********"
                                                       class="form-control" id="password" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Confirm Password
                                                    <span class="symbol required" aria-required="true"></span>
                                                </label>
                                                <input type="password" id="confirm_password" class="form-control" name="confirm_password"
                                                       placeholder="********" value=""   />
                                            </div>
                                            <div class="form-group">
                                                <label> User Profile Image </label>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail profile_img_size" >
                                                        <img width="150px" height="150px" src="{{asset('images/default.jpg')}}" alt="">
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail profile_img_size"
                                                         style="line-height: 20px;">
                                                    </div>
                                                    <div class="user-edit-image-buttons">
													<span class="btn btn-light-grey btn-file">
														<span class="fileupload-new image-filechange">
                                                            <i class="fa fa-picture"></i> Select image
                                                        </span>
														<span class="fileupload-exists image-filechange">
                                                            <i class="fa fa-picture"></i> Change
                                                        </span>
														<input type="file" name="image_url" value="" />
													</span>
                                                        <a href="#" class="btn fileupload-exists btn-light-grey"
                                                           data-dismiss="fileupload">
                                                            <i class="fa fa-times"></i> Remove
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>
                                                By clicking Register, you are agreeing to the Policy and Terms &amp; Conditions.
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <button class="btn btn-teal btn-block" type="submit">
                                                Register <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL FOR CREATE USER -->
                    <!-- PANEL FOR BLOCK USER -->
                    <div id="blocked_user" class="tab-pane {{$tab=='blocked_user' ? 'active':''}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="sample-table-1">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Name Slug</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (!empty($block_users) && count($block_users)>0)
                                            @foreach ($block_users as $key => $blocked_user_list)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $blocked_user_list->name }}</td>
                                                    <td>{{ $blocked_user_list->name_slug }}</td>
                                                    <td>{{ $blocked_user_list->email }}</td>
                                                    <th><span class="label label-danger btn-squared">{{ $blocked_user_list->status }}</span></th>
                                                    <td>
                                                        <button class="btn btn-success btn-xs user_status btn-squared"
                                                                data-user-id="{{$blocked_user_list->id}}" data-status="active">
                                                            Activate
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9">
                                                    <div class="alert alert-success" role="alert">
                                                        <center><h4>No Data Available !</h4></center>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL FOR BLOCK USER -->
                    <!-- PANEL FOR ADMINS -->
                    <div id="active_user" class="tab-pane {{$tab=='active_user' ? 'active':''}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover" id="sample-table-1">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Name Slug</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if (!empty($active_users) && count($active_users)>0)
                                            @foreach ($active_users as $key => $active_users_list)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $active_users_list->name }}</td>
                                                    <td>{{ $active_users_list->name_slug }}</td>
                                                    <td>{{ $active_users_list->email }}</td>
                                                    <td>
                                                        @if($active_users_list->status == "active")
                                                            <span class="label label-success btn-squared">{{ $active_users_list->status }}</span>
                                                        @else
                                                            <span class="label label-danger btn-squared">{{ $active_users_list->status }}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($active_users_list->status=="active")
                                                            <button class="btn btn-danger btn-xs user_status btn-squared"
                                                                    data-user-id="{{$active_users_list->id}}" data-status="deactivate">
                                                                Deactivate
                                                            </button>
                                                        @else
                                                            <button class="btn btn-success btn-xs user_status btn-squared"
                                                                    data-user-id="{{$active_users_list->id}}" data-status="active">
                                                                Activate
                                                            </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="9">
                                                    <div class="alert alert-success" role="alert">
                                                        <center><h4>No Data Available !</h4></center>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- PANEL FOR CREATE USER -->
                    <div id="assign_permission" class="tab-pane {{$tab=='assign_permission' ? 'active':''}}">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="permissions-form"  action="{{url('/partner/assign/permissions')}}" method="post" class="user-form">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>Assign permissions to role</h3>
                                            <hr>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Select User
                                                    <span class="symbol" aria-required="true"></span>
                                                </label>
                                                <select class="form-control search-select assign_user" name="assign_user">
                                                    {{--<option value="" selected="selected"> Please select user</option>--}}
                                                    <option value=""> Please select user</option>
                                                    @if (!empty($active_users) && count($active_users)>0)
                                                        @foreach ($active_users as $key => $active_users_list)
                                                            <option {{isset($_REQUEST['user_id']) && ($_REQUEST['user_id']==$active_users_list->id)? 'selected':''}} value="{{$active_users_list->id}}"> {{ucfirst($active_users_list->name)}} </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>

                                            <input type="hidden" name="assign_user_id" value="{{isset($_REQUEST['user_id'])?$_REQUEST['user_id']:''}}">
                                            <div class="form-group">
                                                <label class="control-label">
                                                    Select User Level
                                                    <span class="symbol" aria-required="true"></span>
                                                </label>
                                                <select class="form-control search-select" name="user_level">
                                                    <option value="">Select user Level</option>
                                                    <option {{(isset($_REQUEST['user_id']) && $ACL->userLevel($_REQUEST['user_id'])==5? 'selected':'')}} value="5"> 5 - Superuser </option>
                                                    <option {{(isset($_REQUEST['user_id']) && $ACL->userLevel($_REQUEST['user_id'])==4? 'selected':'')}} value="4"> 4 </option>
                                                    <option {{(isset($_REQUEST['user_id']) && $ACL->userLevel($_REQUEST['user_id'])==3? 'selected':'')}} value="3"> 3 </option>
                                                    <option {{(isset($_REQUEST['user_id']) && $ACL->userLevel($_REQUEST['user_id'])==2? 'selected':'')}} value="2"> 2 </option>
                                                    <option {{(isset($_REQUEST['user_id']) && $ACL->userLevel($_REQUEST['user_id'])==1? 'selected':'')}} value="1"> 1 </option>

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label for="form-field-select-4">
                                                        Select permissions
                                                    </label>
                                                    <select multiple="multiple" id="form-field-select-4" class="form-control search-select" name="permissions[]">
                                                        <option value="">Select user permission</option>
                                                        @if (!empty($permissions) && count($permissions)>0)
                                                            @foreach($permissions as $permis)
                                                                <option {{isset($_REQUEST['user_id']) && $ACL->getUserPermissionId($_REQUEST['user_id']) && count($ACL->getUserPermissionId($_REQUEST['user_id']))>0 && in_array($permis->id,$ACL->getUserPermissionId($_REQUEST['user_id']))? 'selected':''}} value="{{ $permis->id }}">{{ $permis->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <button class="btn btn-teal btn-block" type="submit">
                                                    Update <i class="fa fa-arrow-circle-right"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL FOR CREATE USER -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('JScript')
<script>
    $(function () {
        var site_url = $('.site_url').val();
        $('#user-form').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                password : {
                    minlength : 4,
                    required : true
                },
                confirm_password : {
                    required : true,
                    minlength : 4,
                    equalTo : "#password"
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                    element.attr("placeholder",error.text());
                }
            }
        });
        $('.user_status').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('user-id');
            var value = $(this).data('status');
            if(value == "active") {
                bootbox.dialog({
                    message: "Are you sure you want to Active this user ?",
                    title: "<i class='glyphicon glyphicon-eye-open'></i> Active !",
                    buttons: {
                        danger: {
                            label: "No!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        success: {
                            label: "Yes!",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'GET',
                                    url: site_url+'/partner/change/user/status/'+id+'/'+value,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );

                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            } else {
                bootbox.dialog({
                    message: "Are you sure you want to Deactivate this user ?",
                    title: "<i class='glyphicon glyphicon-eye-close'></i> Deactivate !",
                    buttons: {
                        danger: {
                            label: "No!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        success: {
                            label: "Yes!",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'GET',
                                    url: site_url+'/partner/change/user/status/'+id+'/'+value,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );
                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            }
        });

        $('.assign_user').on('click', function (e) {
            e.preventDefault();
            user_id = $('.assign_user option:selected').val();
            current_url = site_url+'/partner/user/management?tab=assign_permission';
            if(user_id.length > 0){
                window.location.href = current_url +'&user_id='+user_id;
            }else window.location.href = current_url;
        });
    });
</script>
@endsection
