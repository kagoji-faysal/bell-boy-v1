@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12">
            <ul id="myTab" class="nav nav-tabs tab-bricky">
                <li class="active">
                    <a>
                        <i class="green clip-pencil-2"></i> Edit Partner
                    </a>
                </li>
                <li class="">
                    <a href="{{url('/admin/service/partner/create')}}">
                        <i class="green clip-puzzle-2"></i> Add Partner
                    </a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="add_push">
                    <div class="row">

                        <div class="col-md-4">
                            <form method="post" action="{{url('/admin/service/partner/edit',$partner_info->id)}}" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
{{--                                <input type="hidden" class="form-control" name="partner_image_id" value="{{isset($partner_image->id)?$partner_image->id:''}}">--}}
{{--                                <input type="hidden" class="form-control" name="partner_image_name" value="{{isset($partner_image->image_name)?$partner_image->image_name:''}}">--}}
                                {{--<div class="form-group">
                                    <label for="form-field-23">Partner Name</label>
                                    <input type="text" id="form-field-3" class="form-control" name="partner_name" value="{{isset($partner_info->partner_name)?$partner_info->partner_name:''}}">
                                </div>
                                <div class="form-group">
                                    <label for="form-field-23">Partner Address</label>
                                    <textarea class="form-control" cols="10" rows="5" name="partner_address">{{isset($partner_info->partner_address)?$partner_info->partner_address:''}}</textarea>
                                </div>--}}
                                <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                                    <ul id="myTab" class="nav nav-tabs tab-green">
                                        <li class="active"><a data-toggle="tab" href="#EN">EN</a></li>
                                        <li class=""><a data-toggle="tab" href="#EST">EST</a></li>
                                        <li class=""><a data-toggle="tab" href="#RUS">RUS</a></li>
                                    </ul>
                                    <div class="tab-content" style="margin-bottom: 10px;">
                                        <div id="EN" class="tab-pane fade in active">
                                            <div class="form-group">
                                                <label for="form-field-23">Partner Name *</label>
                                                <input type="text" id="form-field-3" class="form-control" name="partner_name" value="{{isset($partner_info->partner_name)?$partner_info->partner_name:''}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="form-field-23">Partner Address *</label>
                                                <textarea class="form-control" cols="10" rows="5" name="partner_address" required>{{isset($partner_info->partner_address)?$partner_info->partner_address:''}}</textarea>
                                            </div>
                                        </div>
                                        <div id="EST" class="tab-pane fade">
                                            <div class="form-group">
                                                <label for="form-field-23">Partner Name </label>
                                                <input type="text" id="form-field-3" class="form-control" name="partner_name_est" value="{{isset($partner_info->partner_name_est)?$partner_info->partner_name_est:''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="form-field-23">Partner Address *</label>
                                                <textarea class="form-control" cols="10" rows="5" name="partner_address_est">{{isset($partner_info->partner_address_est)?$partner_info->partner_address_est:''}}</textarea>
                                            </div>
                                        </div>
                                        <div id="RUS" class="tab-pane fade">
                                            <div class="form-group">
                                                <label for="form-field-23">Partner Name </label>
                                                <input type="text" id="form-field-3" class="form-control" name="partner_name_rus" value="{{isset($partner_info->partner_name_rus)?$partner_info->partner_name_rus:''}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="form-field-23">Partner Address *</label>
                                                <textarea class="form-control" cols="10" rows="5" name="partner_address_rus">{{isset($partner_info->partner_address_rus)?$partner_info->partner_address_rus:''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="form-field-23">Partner Email</label>
                                    <input type="text" id="form-field-3" class="form-control" name="partner_email" value="{{isset($partner_info->partner_email)?$partner_info->partner_email:''}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="form-field-23">Partner Tax No</label>
                                    <input type="text" id="form-field-3" class="form-control" name="partner_txa_no" value="{{isset($partner_info->partner_txa_no)?$partner_info->partner_txa_no:''}}">
                                </div>
                                <div class="form-group">
                                    <label for="form-field-23">Partner License</label>
                                    <input type="text" id="form-field-3" class="form-control" name="partner_license" value="{{isset($partner_info->partner_license)?$partner_info->partner_license:''}}">
                                </div>
                                <div class="form-group">
                                    <label>
                                        Logo
                                        {{--                                        ( <small>max size: 50mb</small> )--}}
                                    </label>
                                    <div class="fileupload fileupload-exists" data-provides="fileupload"><input type="hidden" value="" name="">
                                        <div class="fileupload-new thumbnail" style="width: 340px; height: 166px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="">
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="width: 336px; height: 166px; line-height: 166px;">
                                            @if(isset($partner_image->id))
                                                <img src="{{URL::asset("/service_images/partner_images/" .$partner_image->image_name)}}">
                                            @endif
                                        </div>
                                        <div>
                                            <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>
                                                <input type="file" name="partner_image" id="partner_image">
                                            </span>
                                            <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                    </div>
                                    <br>
                                </div>

                                <div class="form-group">
                                    <input type="reset" class="btn btn-danger" value="Reset">
                                    <input type="submit" class="btn btn-primary" value="Update">
                                </div>
                            </form>
                        </div>

                        <div class="col-md-8">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-external-link-square"></i>
                                    Partner List
                                    <div class="panel-tools">
                                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">
                                        </a>
                                        <a class="btn btn-xs btn-link panel-close" href="#">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-striped nopadding" id="sample-table-1">
                                            <thead>
                                            <tr>
                                                <th>SL</th>
                                                <th>Partner Address</th>
                                                <th>Email</th>
                                                <th>Address</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($partner_all_data) && count($partner_all_data) > 0)
                                                <?php $page=isset($_GET['page'])? ($_GET['page']-1):0;?>
                                                @foreach($partner_all_data as $key => $list)
                                                    <tr >
                                                        <td>{{ ($key+1+($perPage*$page)) }}</td>
                                                        <td>{{$list->partner_name}}</td>
                                                        <td>{{$list->partner_email}}</td>
                                                        <td>{{$list->partner_address}}</td>
                                                        <td>
                                                            <a href="{{url('/admin/service/partner/edit',$list->id)}}" class="btn btn-green btn-xs tooltips "><i class="fa fa-pencil-square-o " aria-hidden="true" data-toggle1="tooltip" title="Partner Edit"></i></a>
                                                            <a data-class-id="{{$list->id}}" class="btn btn-xs btn-bricky tooltips partner_delete"><i class="fa  fa-trash-o" data-toggle1="tooltip" title="Attribute Class Delete"></i></a>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr class="text-center">
                                                    <td colspan="5">No Data available</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        <?php echo isset($pagination) ? $pagination:"";?>
                                    </div>
                                </div>
                            </div>



                    </div>

                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
@section('JScript')
    <script>
        $(function () {
            var site_url = $('.site_url').val();

            // news delete
            $('.partner_delete').on('click', function (e) {
                e.preventDefault();
                var id = $(this).data('class-id');
                bootbox.dialog({
                    message: "Are you sure you want to delete this Partner ?",
                    title: "<i class='glyphicon glyphicon-trash'></i> Delete !",
                    buttons: {
                        success: {
                            label: "No",
                            className: "btn-success btn-squared",
                            callback: function() {
                                $('.bootbox').modal('hide');
                            }
                        },
                        danger: {
                            label: "Delete!",
                            className: "btn-danger btn-squared",
                            callback: function() {
                                $.ajax({
                                    type: 'GET',
                                    url: site_url+'/admin/service/partner/delete/'+id,
                                }).done(function(response){
                                    bootbox.alert(response,
                                        function(){
                                            location.reload(true);
                                        }
                                    );
                                }).fail(function(response){
                                    bootbox.alert(response);
                                })
                            }
                        }
                    }
                });
            });
        });
        </script>
@endsection
