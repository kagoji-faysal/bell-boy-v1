@extends('layout.master')
@section('content')
    @inject('ACL', 'App\Repositories\RolePermissionForBlade')
    <div class="row">
        <div class="col-md-12">
            @if ($errors->count() > 0 )
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h6>The following errors have occurred:</h6>
                    <ul>
                        @foreach( $errors->all() as $message )
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
            @if (Session::has('errormessage'))
                <div class="alert alert-danger" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('errormessage') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row ">
        <div class="col-sm-12">
            <div class="panel panel-default servicePanel">
{{--                <div class="panel-heading">--}}
{{--                    <i class="fa fa-external-link-square"></i>--}}
{{--                    Text Fields--}}
{{--                    <div class="panel-tools">--}}
{{--                        <a class="btn btn-xs btn-link panel-collapse collapses" href="#">--}}
{{--                        </a>--}}
{{--                        <a class="btn btn-xs btn-link panel-config" href="#panel-config" data-toggle="modal">--}}
{{--                            <i class="fa fa-wrench"></i>--}}
{{--                        </a>--}}
{{--                        <a class="btn btn-xs btn-link panel-refresh" href="#">--}}
{{--                            <i class="fa fa-refresh"></i>--}}
{{--                        </a>--}}
{{--                        <a class="btn btn-xs btn-link panel-expand" href="#">--}}
{{--                            <i class="fa fa-resize-full"></i>--}}
{{--                        </a>--}}
{{--                        <a class="btn btn-xs btn-link panel-close" href="#">--}}
{{--                            <i class="fa fa-times"></i>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="panel-body">
                    <form role="form" class="form-horizontal addServiceItemForm">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Services
                            </label>
                            <div class="col-sm-9 serviceDiv">
                                <input type="text" placeholder="Service name" name="service_pricing_name" class="form-control service_pricing_name">
                                <input type="hidden" name="service_pricing_catalogue_id" class="service_pricing_catalogue_id">
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="form-field-1">
                                Service Categories
                            </label>
                            <div class="col-sm-9">
                                <select multiple="multiple" class="form-control service_pricing_category_id search-select" name="service_pricing_category_id[]">
                                    <option value="">Select Category</option>
                                    @if(!empty($category_all_data) && count($category_all_data)>0)
                                        @foreach($category_all_data as $key => $list)
                                            <option value="{{$list->id}}">{{$list->category_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <span class="errorMsg hide">*Required</span>
                        </div>
                        <div class="panel panel-default addMoreProductsPanel">
                            <div class="panel-heading">
                                <i class="fa fa-external-link-square"></i>
                                Generate Service Items with price
                                <div class="panel-tools">
                                    <a class="btn btn-xs btn-success addMoreProductsBtn" href="#">
                                         <i class="fa fa-plus"></i> Add More Products
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                        Product Title
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" placeholder="Add product short title" name="service_pricing_title[]" class="form-control service_pricing_title">
                                    </div>
                                    <span class="errorMsg hide">*Required</span>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                        Product Description
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea placeholder="Product Description" name="service_pricing_description[]" class="form-control service_pricing_description" spellcheck="false"></textarea>
                                    </div>
                                    <span class="errorMsg hide">*Required</span>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                        Currency
                                    </label>
                                    <div class="col-sm-9">
                                        <select class="form-control service_pricing_currency_unit" name="service_pricing_currency_unit[]">
                                            <option value="">Select Currency</option>
                                            <option value="€">€</option>
                                        </select>
                                    </div>
                                    <span class="errorMsg hide">*Required</span>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">
                                        Price
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" placeholder="0.00" name="service_pricing_unit_cost_amount[]" class="form-control service_pricing_unit_cost_amount">
                                    </div>
                                    <span class="errorMsg hide">*Required</span>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-group-justified col-xs-4 saveServiceItem" value="Save">
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="hide addProductPanelClone">
        <div class="panel panel-default addMoreProductsPanel">
            <div class="panel-heading">
                <i class="fa fa-external-link-square"></i>
                Generate Service Items with price
                <div class="panel-tools">
                    <a class="btn btn-xs btn-success addMoreProductsBtn" href="#">
                        <i class="fa fa-plus"></i> Add More Products
                    </a>
                    <a class="btn btn-xs panel-close btn-danger removeProducts" href="#">
                        <i class="fa fa-minus"></i> Remove This Product
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Product Title
                    </label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="Add product short title" name="service_pricing_title[]" class="form-control service_pricing_title">
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Product Description
                    </label>
                    <div class="col-sm-9">
                        <textarea placeholder="Product Description" name="service_pricing_description[]" class="form-control service_pricing_description" spellcheck="false"></textarea>
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Currency
                    </label>
                    <div class="col-sm-9">
                        <select class="form-control service_pricing_currency_unit" name="service_pricing_currency_unit[]">
                            <option value="">Select Currency</option>
                            <option value="€">€</option>
                        </select>
{{--                        <input type="text" placeholder="currency" name="service_pricing_currency_unit[]" class="form-control service_pricing_currency_unit">--}}
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="form-field-1">
                        Price
                    </label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="0.00" name="service_pricing_unit_cost_amount[]" class="form-control service_pricing_unit_cost_amount">
                    </div>
                    <span class="errorMsg hide">*Required</span>
                </div>
            </div>
        </div>
    </div>


    <!-- start: Form to create categories -->
    <div id="responsive" class="modal fade addServiceModal" tabindex="-1" data-width="600" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>
            <h4 class="modal-title">Create new Service</h4>
        </div>
        <form role="form" class="addServiceForm">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="service_id" class="service_id">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10">
                        <div class="form-group row">
                            <label for="service_name" class="col-sm-4 col-form-label">Service Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control service_name" name="service_name" placeholder="Enter Service Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <textarea class="form-control description" rows="4" name="description" id="description" placeholder="Remarks"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey ">
                    Close
                </button>
                <button type="button" class="btn btn-blue saveServiceBtn">
                    Save Service
                </button>
            </div>
        </form>
    </div>
    <!-- End form--->
@endsection
