<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116245833-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-116245833-2');
        </script>

        <!-- Facebook Pixel Code -->
            <script>
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                    n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t,s)}(window, document,'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '620915522040557');
                fbq('track', 'PageView');
            </script>
            <noscript><img height="1" width="1" style="display:none"
                           src="https://www.facebook.com/tr?id=620915522040557&ev=PageView&noscript=1"
                /></noscript>
    <!-- End Facebook Pixel Code -->


        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{asset('/front-end/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('/front-end/css/glyphicons.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Exo:400,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('/front-end/css/style.css')}}">


        <title>{{isset($page_title) ? $page_title : ''}} | Kalevi Panorama</title>
        @yield('CSS')

    </head>
    <body>
        <div class="page-content">

            @yield('page-content')

        </div>

        @yield('JScript')
        <script src="{{asset('/front-end/js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('/front-end/js/popper.min.js')}}"></script>
        <script src="{{asset('/front-end/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('/front-end/js/moment.js')}}"></script>
        <script src="{{asset('/front-end/js/calender.js')}}"></script>
        <script src="{{asset('/front-end/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{ asset('/front-end/js/isotope_int.js')}}"></script>
		<script src="{{ asset('/front-end/js/front-end.js')}}"></script>
		<script src="{{ asset('/front-end/js/image-slider.js')}}"></script>
        <script src="{{asset('/assets/js/pusher.min.js')}}"></script>

        <input type="hidden" class="site_url" value="{{url('/')}}">
        <input type="hidden" class="expo_token" name="expo_token">

        <script>
            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;
            var pusher = new Pusher( '<?php echo config('notification.PUSHER_APP_KEY');?>',{
                cluster: 'ap2',
                forceTLS: true
            });
            var channel = pusher.subscribe('<?php echo config('notification.PUSHER_APP_CHANNEL');?>');
            console.log('<?php echo isset(\Auth::user()->id) ? \Auth::user()->id : '';?>')
            channel.bind('order-status:' + '<?php echo isset(\Auth::user()->id) ? \Auth::user()->id : '';?>', function(data) {

                // alert(data.message);
                // var notificationCount = $('.noticeCounter').attr('notificationCount');
                // var notificationCountTotal = parseInt(notificationCount) + 1;
                // $('.noticeCounter').attr('notificationCount', notificationCountTotal);
                // $('.noticeCounter').html(notificationCountTotal);
                // var html = '';
                // html+=  '<li>' +
                //     '<a><span class="message" notify_id="'+ data.order_id +'"> ' + data.message + ' </span></a>' +
                //     '</li>'
                // $('.noticeUlList').prepend(html);
            });
        </script>
    </body>
</html>
