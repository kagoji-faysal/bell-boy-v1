
@extends('customer.mail.master-mail')

@section('mail-content')
			<h3 style="margin-top: 0; font-weight: normal; color: #38434d; font-family: 'Exo', sans-serif; font-size: 16px;	margin-bottom: 14px; line-height: 24px;">
			    Hello {{ $order->name}}
		    </h3>

			<p style="margin-top: 0; font-weight: 400; font-size: 14px;	line-height: 22px; color: #7c7e7f; font-family: 'Exo', sans-serif; margin-bottom: 22px;">
				We have received your order. Below are the order number: </p>
{{--			<center>--}}
			<h4 style="background-color: #bcdbee3b; padding:20px; font-weight:bold;margin-top: 0; font-weight: normal; color: #38434d; font-family: 'Exo', sans-serif; font-size: 16px;	margin-bottom: 14px; line-height: 24px; text-align: left;">
                <b>Order #:</b> {{ $order->order_id}}<br>
                <b>Service Name :</b> {{ $item_details[0]['service_pricing_title'] }}<br>
                <b>Service price :</b> {{ $item_cost . ' ' .$item_details[0]['service_pricing_currency_unit'] }}<br>
                <b>Service Provider :</b> {{ $partner_name}}<br>
			</h4>
{{--			</center>--}}
{{--            <p style="margin-top: 0; font-weight: 400; font-size: 14px;	line-height: 22px; color: #7c7e7f; font-family: 'Exo', sans-serif; margin-bottom: 22px;">--}}
{{--                <b>Service Name : </b>{{ $item_details[0]['service_pricing_title'] }}<br>--}}
{{--                <b>Service price : </b>{{ $item_cost . ' ' .$item_details[0]['service_pricing_currency_unit'] }}<br>--}}
{{--                <b>Service Provider : </b>{{ $partner_name}}<br>--}}
{{--            </p>--}}

			<p style="margin-top: 0; font-weight: 400; font-size: 14px;	line-height: 22px; color: #7c7e7f; font-family: 'Exo', sans-serif; margin-bottom: 22px;">
					Our Team will contact you soon. <br />
				 If you didn't make this request then ignore this email.
			</p>

@endsection

