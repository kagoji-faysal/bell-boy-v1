<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
                <div class="top-right links">
                    <a href="{{ url('/language','en') }}">EN</a>
                    <a href="{{ url('/language','est') }}">EST</a>
                </div>

            <div class="content">
                <div class="row">
                    <div class="col-xs-6">
                        <h2>Save Order</h2>
                        <form class="orderForm">
                            <input type="hidden" name="token" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC92MVwvbG9naW4iLCJpYXQiOjE1Nzc3MTkyNzEsImV4cCI6MTU3NzcyMjg3MSwibmJmIjoxNTc3NzE5MjcxLCJqdGkiOiJJZ2VGY1pOWE5qWWtTTUkxIiwic3ViIjoxMCwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.oL8Bap2JtZyZ4vJzd2AOChotc9Zn0HsgTjvFJIZ7Y18">
                            <div class="form-group row">
                                <label for="serviceItemId" class="col-sm-2 col-form-label">Service Items</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="service_item_id">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="order_date" class="col-sm-2 col-form-label">Order Date</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="order_date" name="order_date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="order_time" class="col-sm-2 col-form-label">Order Time</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="order_time" name="order_time">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-group-justified col-xs-4 saveOrder" value="Save">
                            </div>
                        </form>
                    </div>
{{--                </div>--}}

{{--                <div class="row">--}}
                    <div class="col-xs-6">
                        <h2>View Details</h2>
                        <div class="form-group row">
                            <label for="serviceItemId" class="col-sm-2 col-form-label">Service Items</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control service_item_details" name="service_item_id">
                            </div>
                        </div>
                        <form class="detailsForm">
                            <input type="hidden" name="token" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC92MVwvbG9naW4iLCJpYXQiOjE1Nzc3MTkyNzEsImV4cCI6MTU3NzcyMjg3MSwibmJmIjoxNTc3NzE5MjcxLCJqdGkiOiJJZ2VGY1pOWE5qWWtTTUkxIiwic3ViIjoxMCwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.oL8Bap2JtZyZ4vJzd2AOChotc9Zn0HsgTjvFJIZ7Y18">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-group-justified col-xs-4 viewItemDetails" value="Show">
                            </div>
                        </form>


                    </div>

                </div>
                <div class="row">
                    <h2>Details</h2>
                    <div class="viewDetails col-sm-6" style="position: absolute; height: 200px;"></div>
                </div>

                        {{--                <div class="title m-b-md">--}}
{{--                    @lang('message.welcome')--}}
{{--                    <p>Language: {{ \App::getLocale()}}</p>--}}
{{--                </div>--}}
            </div>
        </div>
        <input type="hidden" class="site_url" value="{{url('/')}}">
        <script src="{{ asset('assets/plugins/jQuery-lib/2.0.3/jquery.min.js') }}"></script>
        <script src="{{ asset('js/front-end.js')}}"></script>
    </body>
</html>
