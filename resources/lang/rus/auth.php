<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток входа в систему. Пожалуйста, попробуйте снова через: секунды, секунды.',
    //RegistrationPage
    'reg_page_title' => 'Добавьте ваши данные',
    'reg_page_desc' => 'TechnoLife предоставляет услуги домашней автоматизации, контроля доступа и интеграции.',
    'field-name' => 'имя',
    'field-email' => 'эмаль',
    'field-address' => 'адрес',
    'field-password' => 'пароль',
    'field-confirm_password' => 'Подтвердите пароль',
    'button-next' => 'СЛЕДУЮЩИЙ',
    //TermPage

    'general_term_page_title' => 'Term of use',
    'general_term_page_desc' => 'Techno Life provides home automation, access control and integration services.',
    'button-agree' =>'Agree on these term',
    'sign-agreement' =>'Sign Agreement',
   //SignaturePage
    'signature_page_title'=>'Your signature',
    'signature_page_desc'=>'Techno Life provides home automation, access control and integration services. We are a proud member of USS group and therefore our competence as a group covers: maintenance and administration of property.',
    'button-submit_signature'=>'SUBMIT SIGNATURE',
    'button-clear_signature'=>'CLEAR SIGNATURE',
    'button-sign_signature'=>'SIGN AGREEMENT',
    //LoginPage
    'button-login' => 'LOGIN',
    'login_page_title'=>'Login',
    'forget-password' =>'Forget Password',
	'sign-up' =>'Зарегистрироваться',

    'general_term_page_title' => 'Срок использования',
    'general_term_page_desc' => 'TechnoLife предоставляет услуги домашней автоматизации, контроля доступа и интеграции.',
    'button-agree' =>'Согласен на эти условия',
    //SignaturePage
    'signature_page_title'=>'Ваша подпись',
    'signature_page_desc'=>'TechnoLife предоставляет услуги домашней автоматизации, контроля доступа и интеграции. Мы являемся гордым членом группы USS, и поэтому наша компетенция как группы охватывает: обслуживание и администрирование имущества.',
    'button-submit_signature'=>'ПОДПИСАТЬ ПОДПИСЬ',
    'button-clear_signature'=>'ЯСНАЯ ПОДПИСЬ',
    'button-sign_signature'=>'ПОДПИСАТЬ СОГЛАШЕНИЕ',
    //LoginPage
    'button-login' => 'ВХОД',
    'login_page_title'=>'Войти',
    'forget-password' =>'Забыть пароль',

    //Index Page
    'index_page_title_1' => 'Добро пожаловать в ваш',
    'index_page_title_2' => 'Новый дом',
    'index_page_desc' => 'TechnoLife предоставляет услуги домашней автоматизации, контроля доступа и интеграции. Мы являемся гордым членом группы USS, и поэтому наша компетенция как группы охватывает: обслуживание и администрирование имущества.',
    'top-menu-button-login' => 'Войти',
    'top-menu-button-logout' => 'Выход',
    'top-menu-button-register' => 'запись',
    'top-menu-button-language' => 'язык',
    'right_side_page_browse' =>'Просмотрите наш',
    'right_side_page_service' =>'услуги для вас',
    'button-click_service'=>'НАЖМИТЕ ЗДЕСЬ, чтобы выбрать услуги',
    'top-menu-welcome' => 'желанный',
    //Forget Password
	'forget_pass_page_title'=>'Сбросить пароль',
    'field-new-password' => 'Новый пароль',
	'button-forget-pass' => 'СБРОС',
	'order-history-btn' => 'История заказов',
    'terms_condition' => 'Условия эксплуатации',
    'privacy_policy' => 'политика конфиденциальности',




];
