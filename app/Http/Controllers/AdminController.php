<?php

namespace App\Http\Controllers;


use App\Traits\HasRoleAndPermission;
use App\Image;
use Illuminate\Http\Request;

use Validator;
use Session;
use \App\System;
use \App\Jobs\AdminMailSendingJob;
class AdminController extends Controller
{
    use HasRoleAndPermission;
    /**
     * Class constructor.
     * get current route name for page title
     *
     * @param Request $request;
     */
    public function __construct(Request $request)
    {
        $this->page_title = $request->route()->getName();
        $description = \Request::route()->getAction();
        $this->page_desc = isset($description['desc']) ? $description['desc'] : $this->page_title;
        \App\System::AccessLogWrite();
    }

    public function index()
    {
        $data['page_title'] = $this->page_title;
        return view('admin.index', $data);
    }
    /**
     * Display profile information
     * pass page title
     * Get User data by auth email
     * Get User meta data by joining user
     * Get Products by auth user.
     *
     * @return HTML view Response.
     */
    public function Profile()
    {

        $data['page_title'] = $this->page_title;

        if (isset($_REQUEST['tab']) && !empty($_REQUEST['tab'])) {
            $tab = $_REQUEST['tab'];
        } else {
            $tab = 'panel_overview';
        }
        $data['tab'] = $tab;
        $last_login = (\Session::has('last_login')) ? \Session::get('last_login') : date('Y-m-d H:i:s');
        $data['last_login'] = \Auth::user()->last_login;
        $user_info = \DB::table('users')
            ->where('email', \Auth::user()->email)
            ->first();
        $data['user_info'] = $user_info;
        return view('admin.profile',$data);
    }

    /**
     * Update User Profile
     * if user meta data exist then update else insert user meta data.
     *
     * @param  Request  $request
     * @return Response
     */
    public function ProfileUpdate(Request $request)
    {
        $user_id = \Auth::user()->id;
        $user = \DB::table('users')->where('id', $user_id)->first();
        $v = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
        ]);
        if ($v->fails()) {
            return redirect('/my/profile')->withErrors($v)->withInput();
        }

        try {

            $now = date('Y-m-d H:i:s');
            if (!empty($request->file('image_url'))) {
                $image = $request->file('image_url');
                $img_location = $image->getRealPath();
                $img_ext = $image->getClientOriginalExtension();
                $user_profile_image = \App\Admin::UserImageUpload($img_location, $request->input('email'), $img_ext);
            } else {
                $user_profile_image = $user->user_profile_image;
            }
            $user_info_update_data = array(
                'name' => ucwords($request->input('name')),
                'email' => $request->input('email'),
                'user_profile_image' => $user_profile_image,
                'updated_at' => $now,
            );

            \DB::table('users')->where('id', $user_id)->update($user_info_update_data);

            #eventLog
            \App\System::EventLogWrite('update,users',json_encode($user_info_update_data));

            return redirect('my/profile')->with('message',"Profile updated successfully");

        } catch (\Exception $e) {
            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);
            return redirect('my/profile')->with('errormessage',"Something is wrong!");
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function ProfileImageUpdate(Request $request)
    {
        if (!empty($request->file('image_url'))) {
            $email=\Auth::user()->email;
            $image = $request->file('image_url');
            $img_location=$image->getRealPath();
            $img_ext=$image->getClientOriginalExtension();
            $user_profile_image= \App\Admin::UserImageUpload($img_location, $email, $img_ext);
            $user_new_img = array(
                'user_profile_image' => $user_profile_image,
            );
            try {
                \DB::table('users')
                    ->where('id', \Auth::user()->id)
                    ->update($user_new_img);
                return redirect('my/profile')->with('message',"Profile updated successfully");
            } catch (\Exception $e) {
                $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
                return redirect('my/profile')->with('errormessage',$message);

            }
        }
        return redirect('my/profile');
    }
    /**
     * Update password for specific user
     * checked validation, if failed redirect with error message.
     *
     * @param Request $request
     * @return Response.
     */
    public function UserChangePassword(Request $request)
    {
        $now = date('Y-m-d H:i:s');

        $rules = array(
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
            'current_password' => 'required',
        );

        $v = \Validator::make(\Request::all(), $rules);

        if ($v->fails()) {
            return redirect('/my/profile?tab=change_password')
                ->withErrors($v)
                ->withInput();
        }

        $new_password = $request->input('new_password');
        $confirm_password = $request->input('confirm_password');

        if ($new_password == $confirm_password) {
            if (\Hash::check($request->input('current_password'),
                \Auth::user()->password)) {
                $update_password=array(
                    'password' => bcrypt($request->input('new_password')),
                    'updated_at' => $now
                );
                try {
                    \DB::table('users')
                        ->where('id', \Auth::user()->id)
                        ->update($update_password);


                    #eventLog
                    \App\System::EventLogWrite('update,users-password-change',json_encode($update_password));

                    return redirect('my/profile')->with('message',"Password updated successfully !");

                } catch(\Exception $e) {
                    $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
                    \App\System::ErrorLogWrite($message);
                    return redirect('my/profile')->with('errormessage',"Password update failed !");
                }
            } else {
                return redirect('my/profile?tab=change_password')->with('errormessage',"Current Password Doesn't Match !");
            }
        } else {
            return redirect('my/profile?tab=change_password')->with('errormessage',"Password Combination Doesn't Match !");
        }
    }

    /**
     * Show the form for creating a new user
     * pass page title.
     *
     *@return HTML view Response.
     */
    public function UserManagement()
    {

        $data['page_title'] = $this->page_title;
        if (isset($_REQUEST['tab']) && !empty($_REQUEST['tab'])) {
            $tab = $_REQUEST['tab'];
        } else {
            $tab = 'create_user';
        }
        $data['tab'] = $tab;
        $data['user_info'] = \DB::table('users')->get();
        $data['block_users'] = \DB::table('users')->where('status','deactivate')->get();
        $data['active_users'] = \DB::table('users')->where('status','active')->get();
        $data['roles'] = \DB::table('roles')->get();
        $data['permissions'] = \DB::table('permissions')->get();

        return view('admin.user-management',$data);
    }
    /**
     * Creating new User
     * insert user meta data if data input else insert null to user meta table.
     *
     * @param  Request  $request
     * @return Response
     */
    public function CreateUser(Request $request)
    {
        if ($this->level() >=5) {
            $v = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'roles' => 'required|array',
                'permissions' => 'required|array',
                'password' => 'required',
                'confirm_password' => 'required|same:password',
            ]);
            if ($v->fails()) {
                return redirect()->back()->withErrors($v)->withInput();
            }

            if (count($request->input('roles'))>0 && count($request->input('permissions'))>0) {
                $now=date('Y-m-d H:i:s');
                $slug=explode(' ', strtolower($request->input('name')));
                $name_slug=implode('.', $slug);
                if (!empty($request->file('image_url'))) {
                    $image = $request->file('image_url');
                    $img_location = $image->getRealPath();
                    $img_ext = $image->getClientOriginalExtension();
                    $user_profile_image = \App\Admin::UserImageUpload($img_location, $request->input('email'), $img_ext);
                } else {
                    $user_profile_image='';
                }
                $user_type = ['1'=> 'admin', '2'=>'partner', '3'=>'customer'];

                try {

                    #User Create
                    $user_insert_data =\App\User::create([
                        'name' => ucwords($request->input('name')),
                        'name_slug' => $name_slug,
                        'email' => $request->input('email'),
                        'password' => bcrypt($request->input('password')),
                        'user_profile_image' => $user_profile_image,
                        'login_status' => 0,
                        'user_type' => $user_type[$request->roles[0]],
                        'status' => "active",
                        'created_at' => $now,
                        'updated_at' => $now,
                    ]);
                    #eventLog
                    #updatePassword
                    \App\User::where('id',$user_insert_data->id)->update(['password' => bcrypt($request->input('password'))]);
                    \App\System::EventLogWrite('create,new-users',json_encode($user_insert_data));


                    #Role And Permission
                    $roles = $request->input('roles');
                    $permissions = $request->input('permissions');
                    foreach ($roles as $key => $role){
                        $this->attachUserRole($role,4,$user_insert_data->id);
                    }
                    \App\System::EventLogWrite('create,user role of '.$user_insert_data->id,json_encode($roles));

                    foreach ($permissions as $key => $permission){
                        $this->attachUserPermission($permission,$user_insert_data->id);
                    }

                    \App\System::EventLogWrite('create,permission role of '.$user_insert_data->id,json_encode($roles));


                    #InviationMail
                    $data['user'] = $user_insert_data;
                    $data['password'] = $request->input('password');
                    dispatch(new AdminMailSendingJob($data));

                    return redirect('admin/user/management')->with('message',"User Account Created Successfully !");

                } catch(\Exception $e) {
                    $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
                    \App\System::ErrorLogWrite($message);
                    return redirect('admin/user/management')->with('errormessage',"User Already Exist !");
                }
            } else {
                return redirect('admin/user/management')->with('errormessage',"Role or permission missing");
            }
        } else {
            return redirect('/dashboard')->with('errormessage',"You are not authorized user!");
        }
    }

    /**
     * Change status for individual user.
     *
     * @param int $user_id
     * @param int $status .
     *
     * @return Response.
     */
    public function ChangeUserStatus($user_id, $status)
    {
        $now = date('Y-m-d H:i:s');
        $update = \DB::table('users')
            ->where('id', $user_id)
            ->update(array(
                'status' => $status,
                'updated_at' => $now
            ));
        if ($update) {
            \App\System::EventLogWrite('update,user status '.$user_id,$status);
            echo 'User update successfully.';
        } else {
            echo 'User did not update.';
        }
    }

    /********************************************
 * ## Event Demo
 *********************************************/

    public function DndAddRequest(){
        $data['data'] = ['message'=>'Hello Laravel','url' =>'info'];
        $data['event'] = 'my-event';

        // var_dump($this->PusherDataSend('my-event',$data));
        //var_dump($this->PusherDataSend2('my-channel','my-event',$data));
        echo "Event Trigger";

    }

    //get all data
    public function getAllNotification(){
        $user_id = \Auth::user()->company_id;
        $user_type = \Auth::user()->user_type;
        if($user_type == 'partner'){
            $getAllNotification = \App\Notification::where('company_id', '=', $user_id)->orderBy('created_at', 'DESC')->limit(10)->get()->toArray();
            $newNotification = \App\Notification::where('company_id', '=', $user_id)
                ->where('read', '=', 'unseen')
                ->count();
        } else {
            $getAllNotification = \App\Notification::orderBy('created_at', 'DESC')->limit(10)->get()->toArray();
            $newNotification = \App\Notification::where('read', '=', 'unseen')->count();
        }

        return json_encode(array('data' => $getAllNotification, 'new' => $newNotification));
    }


    //notification list
    public function notificationIndex(){

        return view('notification.index');
    }


    //get order json data
    public function getNotificationJsonList(Request $request){
        if(\Auth::user()->user_type == 'partner'){
            $my_partner_id = \Auth::user()->company_id;
        } else {
            $my_partner_id = '';
        }

        $totalData = \App\Notification::where('user_to_notify', '=', $my_partner_id)
            ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        if($my_partner_id){
            $getAllNotification = \App\Notification::where('company_id', '=', $my_partner_id)->orderBy('created_at', 'DESC')->get()->toArray();
        } else {
            $getAllNotification = \App\Notification::orderBy('created_at', 'DESC')->get()->toArray();
        }
        $data = array();

        foreach ($getAllNotification as $key => $value) {
            $notify_data = array();
            $notify_data = json_decode($value['notify_data'], true);
            $url = '';
            if(isset($notify_data['order_id'])){
                $url = url('/admin/order-details-html/') . '/' . $notify_data['order_id'];
            }
            $data[$key]['sl'] = $key + 1;
            $data[$key]['message'] = isset($notify_data['message']) ? $notify_data['message'] : '';
            $data[$key]['url'] = '<a href="' . $url . '" class="btn btn-xs tooltips btn-primary"><i class="clip-zoom-in"></i></a>';
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return json_encode($json_data);
    }

}
