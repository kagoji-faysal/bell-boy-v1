<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    //
    protected $fillable = ['user_id', 'role_id','level'];
    protected $table = 'role_user';

    public function roles() {
        return $this->belongsTo('\App\Role', 'id');
    }
}
