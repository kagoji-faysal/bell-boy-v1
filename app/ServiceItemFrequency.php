<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceItemFrequency extends Model
{
    //
    use SoftDeletes;
    protected $table = 'service_item_frequencies';

    protected $fillable = [
        'service_item_id',
        'service_frequency_id',
    ];
}
