<?php

namespace App\Providers;

use App\Role;
use Illuminate\Support\ServiceProvider;

class RoleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Role::deleted(function($roles) {

            foreach($roles->role_user as $role){
                $role->delete();
            }
        });
    }
}
