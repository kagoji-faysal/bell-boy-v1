<?php

namespace App\Providers;

use App\Observers\PermissionObserver;
use App\Permission;
use Illuminate\Support\ServiceProvider;

class PermissionModelServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Permission::deleted(function($permissions) {

            foreach($permissions->permission_role as $permit){
                $permit->delete();
            }
        });
    }
}
