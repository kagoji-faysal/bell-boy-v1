<?php

namespace App\Providers;

use App\ServicesCategory;
use Illuminate\Support\ServiceProvider;

class CategoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        ServicesCategory::deleted(function($category) {
            foreach($category->service_item as $service_item){
                $service_item->delete();
            }

            foreach($category->service_item_pricing as $service_price){
                $service_price->delete();
            }

            /*foreach($category->service_catalogue as $service_catalog){
                $service_catalog->delete();
            }*/
        });


    }
}
