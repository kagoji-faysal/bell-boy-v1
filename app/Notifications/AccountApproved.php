<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class AccountApproved extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */

    protected $order;
    public function __construct($order){
        $this->order=$order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [ExpoChannel::class];
    }



    public function toExpoPush($notifiable){
        $message = "Message : ".$notifiable;
        \App\System::ErrorLogWrite($message);
        return ExpoMessage::create()
            ->badge(1)
            ->title("Hello World!")
            ->enableSound()
            ->body("Hello World!")
            ->setChannelId("chat-messages")
            ;
    }
}
