<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCatalogue extends Model
{
    //
    use SoftDeletes;
    protected $table = 'service_catalogue';

    protected $fillable = [
        'service_name',
        'services_categories_id',
        'service_name_slug',
        'service_description',
        'status'
    ];

    public function image(){
        return $this->morphMany(Image::class, 'imageable');
    }

    public function service_category() {
        return $this->belongsTo('\App\ServiceCategory', 'id');
    }
}
