<?php

namespace App\Traits;

use App\User;
use Pusher\Pusher;

trait NotificationSending
{

    /**
     *Pusher Message Send function.
     *
    * @param string $event_name
    * @param array  $data
    *
    * @return json
    */

    public function PusherDataSend($event_name,$data){

        try {

            $oauthClient = [
                'PUSHER_APP_KEY'                => config('notification.PUSHER_APP_KEY'),
                'PUSHER_APP_SECRET'             => config('notification.PUSHER_APP_SECRET'),
                'PUSHER_APP_ID'                 => config('notification.PUSHER_APP_ID'),
                'PUSHER_APP_CHANNEL'            => config('notification.PUSHER_APP_CHANNEL')
            ];

            if(empty($oauthClient['PUSHER_APP_KEY'])
                || empty($oauthClient['PUSHER_APP_SECRET'])
                || empty($oauthClient['PUSHER_APP_ID'])
                || empty($oauthClient['PUSHER_APP_CHANNEL'])){

                throw new \Exception('Pusher credentials missing!!');

            }
            $options = array(
                'cluster' => 'ap2',
                'useTLS' => true
            );

            //new Pusher()
            $pusher = new Pusher(
                $oauthClient['PUSHER_APP_KEY'],
                $oauthClient['PUSHER_APP_SECRET'],
                $oauthClient['PUSHER_APP_ID'],
                $options
            );

            $response = $pusher->trigger($oauthClient['PUSHER_APP_CHANNEL'],$event_name,$data);
            $message = "Channel: $oauthClient[PUSHER_APP_CHANNEL] | Event: $event_name | Response : $response | Data: ".json_encode($data);
            \App\System::CustomLogWritter("Pushlog","push_send_log",$message);

            return $response;

        }catch(\Exception $e){

            $message = "Message : ".$e->getMessage().", File : ".$e->getFile().", Line : ".$e->getLine();
            \App\System::ErrorLogWrite($message);

            return \Response::json(['error-message'=>'Something wrong happened in Puhser']);
        }
    }

}