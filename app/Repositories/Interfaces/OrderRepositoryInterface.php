<?php

namespace App\Repositories\Interfaces;


interface OrderRepositoryInterface
{

    public function getOrderListForDataTable($start, $limit);
    public function getOrderListForDataTableSearch($start, $limit, $search);
    public function getOrderListByPartnerIdForDataTable($start, $limit, $my_partner_id);
    public function getOrderListByPartnerIdForDataTableSearch($start, $limit, $search, $my_partner_id);
    public function resizeOrderArrayForJson($orderData, $hasViewPermission);
    public function getOrderDetailsByOrderId($order_id);
    public function getOrderDetailsByOrderIdForPartner($order_id, $partner_id);
//    public function getOrderItemName($type_value_id);
}
