<p align="center">Services development</p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Installation
* Clone the git repository
* add config/database.php and storage directory
* change config/database.php like below

'mysql' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3307'),
            'database' => env('DB_DATABASE', 'technolife_services'),
            'username' => env('DB_USERNAME', 'root'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => false,
        ],
'mysql2_logger' => [
    'driver' => 'mysql',
    'host' => env('DB_HOST_FOR_LOG', '127.0.0.1'),
    'port' => env('DB_PORT_FOR_LOG', '3307'),
    'database' => env('DB_DATABASE_FOR_LOG', 'technolife_services_logger'),
    'username' => env('DB_USERNAME_FOR_LOG', 'root'),
    'password' => env('DB_PASSWORD_FOR_LOG', ''),
    'unix_socket' => env('DB_SOCKET', ''),
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix' => '',
    'strict' => false,
    'engine' => null,
],

* make changes .env file like below

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3307
DB_DATABASE=technolife_services
DB_USERNAME=root
DB_PASSWORD=

DB_CONNECTION_FOR_LOG=mysql
DB_HOST_FOR_LOG=127.0.0.1
DB_PORT_FOR_LOG=3307
DB_DATABASE_FOR_LOG=technolife_services_logger
DB_USERNAME_FOR_LOG=root
DB_PASSWORD_FOR_LOG=

* Composer Update
* Migrate the database table
* Run the database seeder command
 php artisan db:seed --class=UsersTableSeeder
* Login Information
Email: admin@technolife.ee
Password: 1234

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Kagoji Faysal via [kagoji@technolife.ee](mailto:kagoji@technolife.ee). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).