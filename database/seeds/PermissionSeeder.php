<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Permission Types
         *
         */
        $Permissionitems = [
            [
                'name'        => 'view',
                'slug'        => 'view',
                'description' => 'Can view users',
            ],
            [
                'name'        => 'create',
                'slug'        => 'create',
                'description' => 'Can create new users',
            ],
            [
                'name'        => 'edit',
                'slug'        => 'edit.users',
                'description' => 'Can edit users',
            ],
            [
                'name'        => 'delete',
                'slug'        => 'delete',
                'description' => 'Can delete users',
            ],
        ];
        $now = date('Y-m-d H:i:s');
        foreach ($Permissionitems as $Permissionitem) {

            $newPermissionitem = \App\Permission::where('slug', '=', $Permissionitem['slug'])->first();
            if ($newPermissionitem === null) {
                $newPermissionitemInsert = \App\Permission::create([
                    'name'          => $Permissionitem['name'],
                    'slug'          => $Permissionitem['slug'],
                    'description'   => $Permissionitem['description'],
                    'created_at'   => $now,
                    'updated_at'   => $now,

                ]);

                $roles = \App\Role::all();
                if(count($roles)>0){
                    foreach ($roles as $role){
                        $permission_role = \DB::table('permission_role')->insert([
                            'role_id' => $role->id,
                            'permission_id' => $newPermissionitemInsert->id,
                            'created_at'   => $now,
                            'updated_at'   => $now,
                        ]);
                    }
                }
            }

        }
        echo "\e[32mSeeding:\e[0m DefaultPermissionitemsTableSeeder - \r\n";

        /*
         * Attach Permission
         *
         */
        $attachpermission = \App\User::where('email', 'admin@technolife.ee')->first();
        $permissions = \App\Permission::all();
        if(isset($attachpermission->id)&&count($permissions)>0){
            $attachpermission->permission()->sync($permissions);
        }
        echo "\e[32mSeeding:\e[0m AdminPermissionSeeder\r\n";
    }
}
