<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLangFieldInServiceFrequenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_frequencies', function (Blueprint $table) {
            $table->string('frequency_name_est')->after('frequency_name')->nullable();
            $table->string('frequency_name_rus')->after('frequency_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_frequencies', function (Blueprint $table) {
            //
        });
    }
}
