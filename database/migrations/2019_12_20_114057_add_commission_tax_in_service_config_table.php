<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommissionTaxInServiceConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_config', function (Blueprint $table) {
            $table->decimal('service_tax_price',8,2)->after('service_pricing_currency_unit')->default(0.00);
            $table->decimal('service_commission_price',8,2)->after('service_pricing_currency_unit')->default(0.00);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_config', function (Blueprint $table) {
            //
        });
    }
}
