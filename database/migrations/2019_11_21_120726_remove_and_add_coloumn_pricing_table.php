<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveAndAddColoumnPricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_item_pricing', function (Blueprint $table) {
            $table->dropColumn('service_pricing_title');
            $table->dropColumn('service_pricing_description');
            $table->dropColumn('service_pricing_currency_unit');
            $table->dropColumn('service_pricing_unit_cost_amount');
        });

        Schema::table('service_item_pricing', function (Blueprint $table) {
            $table->bigInteger('service_config_id')->unsigned()->default(0)->after('service_pricing_partner_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_item_pricing', function (Blueprint $table) {
            $table->string('service_pricing_title');
            $table->mediumText('service_pricing_description');
            $table->string('service_pricing_currency_unit'); //Dollar,euro
            $table->decimal('service_pricing_unit_cost_amount',8,2);
        });


        Schema::table('service_item_pricing', function (Blueprint $table) {
            $table->dropColumn('service_config_id');
        });
    }
}
