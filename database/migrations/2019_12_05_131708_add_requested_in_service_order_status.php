<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRequestedInServiceOrderStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE service_orders CHANGE COLUMN service_order_status service_order_status ENUM('requested', 'accepted', 'cancel', 'rejected')");
    }

    public function down()
    {
        DB::statement("ALTER TABLE service_orders CHANGE COLUMN service_order_status service_order_status ENUM('accepted', 'cancel', 'rejected')");
    }


}
