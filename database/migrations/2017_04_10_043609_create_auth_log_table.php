<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2_logger')->defaultStringLength(191);
        Schema::connection('mysql2_logger')->create('auth_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('auth_client_ip');
            $table->string('auth_user_id');
            $table->string('auth_browser');
            $table->string('auth_platform');
            $table->string('auth_type');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auth_log');
    }
}
