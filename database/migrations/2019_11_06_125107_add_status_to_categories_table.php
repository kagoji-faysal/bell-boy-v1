<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_categories', function (Blueprint $table) {
            //
            $table->enum('status', ['active', 'inactive','draft','pending']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_categories', function (Blueprint $table) {
            //
            Schema::dropIfExists('services_categories');
        });
    }
}
