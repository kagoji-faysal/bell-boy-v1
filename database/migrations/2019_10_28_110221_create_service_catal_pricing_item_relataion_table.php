<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceCatalPricingItemRelataionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_items', function (Blueprint $table) {
//            $table->foreign('service_item_catalogue_id')->references('id')->on('service_catalogue')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_item_category_id')->references('id')->on('services_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_item_partner_id')->references('id')->on('service_partner_info')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::table('service_item_pricing', function (Blueprint $table) {

            $table->foreign('service_pricing_item_id')->references('id')->on('service_items')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_pricing_category_id')->references('id')->on('services_categories')->onDelete('cascade')->onUpdate('cascade');
//            $table->foreign('service_pricing_catalogue_id')->references('id')->on('service_catalogue')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_pricing_partner_id')->references('id')->on('service_partner_info')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('');
    }
}
