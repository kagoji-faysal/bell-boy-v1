<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id'); // set relationship in user model
            $table->bigInteger('user_to_notify'); //the user who will recive this notification
            $table->string('notify_type'); //follow ,comments etc
            $table->text('notify_data'); //follow id ,comment id etc ,you can set relationship in model about this
            $table->enum('read', ['seen','unseen'])->default('unseen');
            $table->softDeletes();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void notifications
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
