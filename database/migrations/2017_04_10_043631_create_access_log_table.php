<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2_logger')->defaultStringLength(191);
        Schema::connection('mysql2_logger')->create('access_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('access_client_ip');
            $table->string('access_user_id');
            $table->longText('access_message');
            $table->string('access_browser');
            $table->string('access_platform');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access_log');
    }
}
