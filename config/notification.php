<?php
return [
    'PUSHER_APP_KEY' => env('PUSHER_APP_KEY', 'f969314210ca03f33d3e'),
    'PUSHER_APP_SECRET' => env('PUSHER_APP_SECRET', 'c0b067cc6ebe85ad24e7'),
    'PUSHER_APP_ID' => env('PUSHER_APP_ID', '771026'),
    'PUSHER_APP_CHANNEL' => env('PUSHER_APP_CHANNEL', 'my-channel')
];